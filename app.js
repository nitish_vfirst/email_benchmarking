var express = require('express')
var app = express()
const mysql = require('mysql2/promise');
const bluebird= require('bluebird');

const db =  mysql.createPool ({
    host: "localhost",
    user: "root",
    password: "Abc@1155",
    database: "emails",
    Promise: bluebird
});

async function insert(){
  var start = Date.now()
    console.log("==============================  Batch Insertion of 20000000 in 1000000 batches Started  ===========================")
for(let i = 0;i<=18;i++){
  let arr = []
 for(let j = 1 ;j<=1000000;j++){
   arr.push(
        [ `email${j+i*1000000}@gmail.com`, `bounce`, j+i*1000000]
 )
  
 }
 let insert = await db.query(`INSERT INTO emails (email , reason ,error_code ) VALUES ?`,[arr])
  console.log("=================================================" + i+ " Data Inserted of 20000000 in 1000000 batches===================================")		
  
}
}
insert();

app.listen(3000, () => {
    console.log(`Server started 3000...`)
  })